import random
from flask import Flask, request
import os
import psycopg2
import requests

destino_numero = '/tmp'
menor_numero = 1
maior_numero = 10
app = Flask(__name__)


@app.route('/gerar-numero', methods=['GET'])
def gera_numero():
    numero_aleatorio = random.randint(menor_numero, maior_numero)
    print(numero_aleatorio)
    file = open(destino_numero + '/resposta.txt', 'w')
    file.write(str(numero_aleatorio))
    file.close()
    response = "<font color=#0000BB size=+1>Número gerado, pode tentar com <i>{}adivinhar/#</i><BR>" \
               "Onde # é um número entre <b>{}</b> e <b>{}</b></font>".format(request.url_root, menor_numero, maior_numero)
    return response

cf_port = os.getenv("PORT")

@app.route('/adivinhar/<chute>', methods=['GET'])
def adivinha_numero(chute):
    file = open(destino_numero + '/resposta.txt', 'r')
    resposta = file.readline()
    if int(chute) == int(resposta):
        return "<font color=#009900><b>Acertou, miserávi</b></font>"
    elif int(chute) < int(resposta):
        return "<font color=#FF0000><b>Errou! Tente um numero maior</b></font>"
    else:
        return "<font color=#FF0000><b>Errou! Tente um numero menor</b></font>"


@app.route('/testaservico', methods=['GET'])
def testaservico():
    auth = {  "login": "string",  "senha": "string" }
    resp = requests.post('http://aplicacoes.mds.gov.br/integradorsei/api/autenticar',json=auth)
    print(format(resp.status_code))
    return "auth"


@app.route('/conecta', methods=['GET'])
def conecta():
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect("dbname=zabbixdb user=zabbix host=supgpd13.mds.net password=8QN82dyY")

        # create a cursor
        if conn is not None:
            cur = conn.cursor()


   # execute a statement
        print('PostgreSQL database version:')
        if conn is not None:
            cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

       # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

    return format(db_version)


if __name__ == '__main__':
	if cf_port is None:
		app.run(host='0.0.0.0', port=5000, debug=True)
	else:
		app.run(host='0.0.0.0', port=int(cf_port), debug=True)
